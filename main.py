from flask import Flask, render_template, request
from loop import input_number_loop
import json
app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('mainhtml.html')

@app.route('/background_process_loop', methods=['GET', 'POST'])
def background_process_loop():
    if request.method == 'POST':
        args = request.form.to_dict(flat=True)
        looped_list = input_number_loop(int(args['input_number']))
        return json.dumps(looped_list)


if __name__ == '__main__':
    app.run(debug=True)