$("#startButton").on("click", function() {
    console.log('gets here');
    let inputNumber = $("#numberInput").value();
    $.ajax({
        type: "POST",
        url: "/background_process_loop",
        data: {
            input_number: inputNumber
        },
        dataType: "json",
        complete: function() {
            console.log("completed");
        },
        success: function(response) {
            console.log("success", response);
        },
        error: function(e, r, status) {
            console.log("error", status);
        }
    });
});

